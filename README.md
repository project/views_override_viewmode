# About

This module lets you make a views block display's _view mode_ configurable
for each individual block, via the block's configuration settings.

This module depends and builds upon **ctools_views** (part of
[https://drupal.org/project/ctools](ctools)), which lets you expose 
additional views configuration options as individual block 
configuration options.

## Use case

* A site's 'Articles' content type has 4 custom view modes
  (`small`,`medium`, `large`, `extra_large`)
* Editors use Layout Builder to place 'recent articles' blocks all over the site.
* Editors should be able to select which view mode to use for any given block.

## Installation

Install with composer as you would any contributed module.

This module itself has no configuration options; it only provides additional 
configuration options to views block displays.

## Usage

* Add a new block display to a view.
* Set the block display's row type to `content` (as opposed to _fields_).
* Select a view mode to use by default.
* in the block display's `block settings > Allow settings`, enable `Select view mode`.
* Save the view.
* Place an instance of this views block somewhere.
* In the block's configuration options, select the view mode of your choice.
* Save the block.

## Limitations

* It's not (yet) possible to limit the choice of available view modes; all the 
  view modes for the entity type on which the view is based are available.

* This module follows the ctools_views logic/architecture: 
  * Ctools provides a views block display plugin that extends the core views block display plugin, 
    and then alters the views block plugin definition to point to `Drupal\ctools_views\Plugin\Display\Block`
    rather than `Drupal\views\Plugin\views\display\Block`.
  * This module does exactly the same, but extends the ctools_views views block display plugin so as 
    not to lose any of this functionality. This works fine, but it means it cannot coexist with other 
    modules that do the same since the views block plugin definition can only point to one specific class.
  * See also: `views_block_override_viewmode_views_plugins_display_alter()`. 

## Troubleshooting

Block displays that were created _before_ ctools_views was installed seem to be
missing the expanded 'allow options' that ctools_views offers. 
If this is the case, create a new block display instead of trying to configure 
the pre-ctools_views one.

## Credits

* Original developer and maintainer: [jpoesen](https://drupal.org/u/jpoesen)
* Supporting organisations:
  * [London Borough of Hammersmith & Fulham](https://lbhf.gov.uk)
