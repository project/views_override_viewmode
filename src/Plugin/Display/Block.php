<?php

namespace Drupal\views_override_viewmode\Plugin\Display;

use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\Block\ViewsBlock;
use Drupal\views\Plugin\ViewsHandlerManager;
use Drupal\ctools_views\Plugin\Display\Block as CtoolsBlock;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a Block display plugin.
 *
 * Allows individual blocks to override the configured view mode selection
 * of the block display they're based on.
 */
class Block extends CtoolsBlock {

  /**
   * The entity display Repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * Constructs a new Block instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager.
   * @param \Drupal\Core\Block\BlockManagerInterface $block_manager
   *   The block manager.
   * @param \Drupal\views\Plugin\ViewsHandlerManager $filter_manager
   *   The views filter plugin manager.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, BlockManagerInterface $block_manager, ViewsHandlerManager $filter_manager, Request $request, EntityDisplayRepositoryInterface $entity_display_repository) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $block_manager, $filter_manager, $request);

    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.block'),
      $container->get('plugin.manager.views.filter'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('entity_display.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function optionsSummary(&$categories, &$options) {
    parent::optionsSummary($categories, $options);

    $row_type = $this->getOption('row')['type'];

    // If row type is not 'content' (entity:{id}), handle the case
    // of no options being present, and then return early.
    if (strpos($row_type, 'entity:') === FALSE) {
      if (empty($options['allow']['value'])) {
        $options['allow']['value'] = $this->t('None');
      }
      return;
    }

    // Retrieve the allowed options as defined by the CtoolsBlock
    // parent class (format: comma-separated list).
    $rendered_selected_options = $options['allow']['value'];

    // Retrieve the available and selected options.
    $available_options = $this->getOption('allow');
    $selected_options = array_filter($available_options);

    // Replace the summary list or add the view mode label to the list.
    if (isset($selected_options['entity_view_mode'])) {
      if (empty($rendered_selected_options)) {
        $rendered_selected_options = $this->t('View mode');
      }
      else {
        $rendered_selected_options .= ", {$this->t('View mode')}";
      }
    }

    $options['allow']['value'] = empty($rendered_selected_options) ?
      $this->t('None') : $rendered_selected_options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    // Only add view_mode selector if row type = content (entity:{id}).
    $row_type = $this->getOption('row')['type'];
    if (strpos($row_type, 'entity:') === FALSE) {
      return;
    }

    $form['allow']['#options']['entity_view_mode'] = $this->t('Select view mode.');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm(ViewsBlock $block, array &$form, FormStateInterface $form_state) {
    $form = parent::blockForm($block, $form, $form_state);

    $row_options = $this->getOption('row');
    // Return early if row type != 'content' (entity:{id}).
    if (strpos($row_options['type'], 'entity:') === FALSE) {
      return $form;
    }

    $allow_settings = array_filter($this->getOption('allow'));
    $block_configuration = $block->getConfiguration();

    // Provide Entity view mode selector.
    if (!empty($allow_settings['entity_view_mode'])) {

      // Retrieve the entity type this view is based on.
      $view = $block->getViewExecutable();
      $entity_type = $view->getBaseEntityType();
      $entity_type_id = ($entity_type !== FALSE) ? $entity_type->id() : 'node';

      // $view_modes = \Drupal::service('entity_display.repository')
      $view_modes = $this->entityDisplayRepository
        ->getViewModes($entity_type_id);

      // Retrieve the view mode that this display is using, assuming it is
      // configured with format set to 'content' rather than 'fields'.
      $view_mode_from_display = '';
      if (!empty($row_options) && isset($row_options['options']['view_mode'])) {
        $view_mode_from_display = $row_options['options']['view_mode'];
      }

      $view_mode_options = [];
      foreach ($view_modes as $view_mode_id => $view_mode) {
        $view_mode_options[$view_mode_id] = $view_mode['label'];
      }

      $default_value = isset($block_configuration['entity_view_mode']) ?
        $block_configuration['entity_view_mode'] :
        $view_mode_from_display;

      $form['override']['entity_view_mode'] = [
        '#type' => 'select',
        '#options' => $view_mode_options,
        '#title' => $this->t('View mode'),
        '#default_value' => $default_value,
        '#description' => $this->t('Select which view mode to use.'),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit(ViewsBlock $block, $form, FormStateInterface $form_state) {
    parent::blockSubmit($block, $form, $form_state);
    $configuration = $block->getConfiguration();
    $allow_settings = array_filter($this->getOption('allow'));

    // Save entity view mode config.
    if (!empty($allow_settings['entity_view_mode'])) {
      $configuration['entity_view_mode'] = $form_state
        ->getValue(['override', 'entity_view_mode']);
    }

    $block->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function preBlockBuild(ViewsBlock $block) {
    parent::preBlockBuild($block);

    $allow_settings = array_filter($this->getOption('allow'));
    $config = $block->getConfiguration();
    list(, $display_id) = explode('-', $block->getDerivativeId(), 2);

    // Change view mode settings based on block configuration.
    if (!empty($allow_settings['entity_view_mode']) && isset($config['entity_view_mode'])) {
      $row_options = $this->view->display_handler->getOption('row');
      $row_options['options']['view_mode'] = $config['entity_view_mode'];
      $this->view->display_handler->setOption('row', $row_options);
    }

  }

}
